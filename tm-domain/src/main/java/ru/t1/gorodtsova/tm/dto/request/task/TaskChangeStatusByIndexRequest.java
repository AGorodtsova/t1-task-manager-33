package ru.t1.gorodtsova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIndexRequest;
import ru.t1.gorodtsova.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        super(token, index);
        this.status = status;
    }

}
