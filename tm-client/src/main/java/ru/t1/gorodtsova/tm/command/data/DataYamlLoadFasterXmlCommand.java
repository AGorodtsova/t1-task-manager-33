package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.domain.DataYamlLoadFasterXmlRequest;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    private final String NAME = "data-load-yaml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        getDomainEndpoint().loadDataYamlFasterXml(request);
    }

}
